﻿package com.example.AccountRestApi.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.AccountRestApi.Model.Account;
import com.example.AccountRestApi.Service.AccountService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AccountController {
    @Autowired
    private AccountService service;

    @GetMapping("/account")

    public List<Account> getAccounts() {
        return service.createAccount();
    }
}
