﻿package com.example.AccountRestApi.Model;

public class Account {
    private int id;
    private String name;
    private double balance = 0.0;

    public Account(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Account(int id, String name, double balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double deposit(double amount) {
        this.balance += amount;
        return this.balance;
    }

    public double widthdraw(double amount) {
        if (amount <= this.balance) {
            this.balance -= amount;
            return this.balance;
        } else {
            System.out.println("Amount exceeded balance");
            return this.balance;
        }
    }

    @Override
    public String toString() {
        return "Account [id=" + id + ", customer=" + this.name + ", balance=" + balance + "]";
    }
}
