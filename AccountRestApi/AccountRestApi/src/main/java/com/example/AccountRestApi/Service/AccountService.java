﻿package com.example.AccountRestApi.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.AccountRestApi.Model.Account;

@Service
public class AccountService {
    public List<Account> createAccount() {
        ArrayList<Account> accountList = new ArrayList<>()
        Account account1 = new Account(3, "tam");
        Account account2 = new Account(3, "tam2", 200.00);
        Account account3 = new Account(3, "tam2", 400.00);
        accountList.addAll(Arrays.asList(account1,account2,account3));
        return accountList;
    }
}
